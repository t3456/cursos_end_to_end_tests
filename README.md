# Teste de consulta de cursos por professor utilizando Cypress. 

> Acessar a plataforma de cursos e realizar testes end to end.


| Recurso |
| ------ |
| NodeJs |
| NPM |

# Instalação

Faça o clone ou download do projeto.
Acesse o diretório "cursos_end_to_end_tests" e rode o comando:

```
  npm install
```

Ao terminar a instalação, rode o comando abaixo para executar os testes em modo janela. 
```
  npx cypress open
```
Quando a aplicação estiver rodando acesse a janela do cypress. Esta janela abre de forma padrão na aba "Tests".
Acesse o caminho: "INTEGRATIONS TESTS > specs" e clique no arquivo "ConsultaProfessor.spec.js" 

Caso preferir executar os testes em modo headless basta rodar o comando abaixo.

```
  npx cypress run --record --key dcb80f2c-ca91-4653-8fcc-6d62da0f5a9c --browser chrome
```  
