const elementosLogin = {
    email: '[id="login_email"]',
    senha: '[id="login_senha"]',
    botaoLogin: 'Login',
};

export default elementosLogin;