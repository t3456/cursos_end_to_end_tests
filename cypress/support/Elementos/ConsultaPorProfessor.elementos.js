// a professora "Ena Loiola" não existia na listagem
const elementosConsultaProf = {
    buscaPorProf: 'Por professor',
    nomeProf: 'Ena Smith',
    valorParcelaNaListagem: '[class="card-prod-price"]',
    detalheCurso: 'Detalhes',
    valorParcelaEmDetalhes: '[class="cur-details-shopping-installments"]',
    valorTotalCurso: '[class="value"]',
    botaoComprar: 'Comprar',
    cursoNoCarrinho: '[class="column -description -colored"]',
    esvaziarCarrinho: '[class="cart-exclude"]',
    carrinhoVazio: '[class="ui-alert -warning"]',
    botaoContinuarComprando: 'Continuar comprando',
    botaoOrdernarPreco: 'Preço',
    valorTotalCompra: '[class="_font-size-x-x-large"]',
};

export default elementosConsultaProf;