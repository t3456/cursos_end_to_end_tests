/// <reference types="cypress" />
import elementosLogin from '../support/Elementos/Login.elementos';

Cypress.Commands.add("realizarLogin", (usuario, senha)=>{
    cy.get(elementosLogin.email).type(usuario);
    cy.get(elementosLogin.senha).type(senha);
    cy.contains(elementosLogin.botaoLogin).click();
});