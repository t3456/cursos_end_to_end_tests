/// <reference types="cypress" />
import elementosConsultaProf from '../../support/Elementos/ConsultaPorProfessor.elementos';
import dadosConsultaProf from '../../support/Dados/ConsultaPorProfessor.dados';
import dadosLogin from '../../support/Dados/Login.dados';


describe('Realizar busca de curso por professor e:', ()=>{
    
    beforeEach(()=>{
        cy.visit('/');

        cy.contains(elementosConsultaProf.buscaPorProf).click();
        cy.contains(elementosConsultaProf.nomeProf).click();
    });

    it('Verificar se o valor da parcela na listagem é igual ao valor apresentado em detalhes do curso. '+
    'Alem disso, verificar se o valor total é igual a soma de todas as parcelas', ()=>{
        cy.get(elementosConsultaProf.valorParcelaNaListagem).first().contains(dadosConsultaProf.dadosAssert.valorCurso);
        cy.contains(elementosConsultaProf.detalheCurso).first().click();
        cy.get(elementosConsultaProf.valorParcelaEmDetalhes).contains(dadosConsultaProf.dadosAssert.valorCurso);
        cy.get(elementosConsultaProf.valorTotalCurso).contains(dadosConsultaProf.dadosAssert.valorTotal);
    });

    it('Selecionar curso, realizar login para adiciona-lo ao carrinho e verificar item selecionado. '+
    'Alem diso, esvaziar carrinho para futuras compras', ()=> {
        cy.contains(elementosConsultaProf.botaoComprar).first().click();
        cy.realizarLogin(dadosLogin.usuario, dadosLogin.senha);
        cy.get(elementosConsultaProf.cursoNoCarrinho).contains('ABIN (Agente de Inteligência) Inglês - 2021 (Pré-Edital)');
        cy.get(elementosConsultaProf.esvaziarCarrinho).click();
        cy.get(elementosConsultaProf.carrinhoVazio).contains('Seu carrinho está vazio!');
    });

    it('Adicionar dois cursos no carrinho e verificar se o valor total da compra está correto', ()=>{
        cy.contains(elementosConsultaProf.botaoComprar).first().click();
        cy.realizarLogin(dadosLogin.usuario, dadosLogin.senha);
        cy.contains(elementosConsultaProf.botaoContinuarComprando).click();
        cy.contains(elementosConsultaProf.buscaPorProf).click();
        cy.contains(elementosConsultaProf.nomeProf).click();
        cy.contains(elementosConsultaProf.botaoOrdernarPreco).click();
        cy.contains(elementosConsultaProf.botaoComprar).first().click({force: true});
        cy.get(elementosConsultaProf.valorTotalCompra).contains('R$ 156,00');
    });
});